const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Items = sequelize.define(
  "Items",
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    paymentMethod: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    trackingCode: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    amount: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    timestamps: false,
  }
);

Items.associate = function (models) {
  Items.belongsTo(models.User);
};

module.exports = Items;
