const Items = require("../models/Items");
const User = require("../models/User");

async function create(req, res) {
  try {
    const items = await Items.create(req.body);
    return res
      .status(201)
      .json({ message: "usuario cadastrado com sucesso", Items: items });
  } catch (err) {
    return res.status(500).json(err);
  }
}

async function index(req, res) {
  try {
    const items = await Items.findAll();
    return res
      .status(200)
      .json({ message: "todos os usuarios listados", Items: items });
  } catch (error) {
    return res.status(500).json(error);
  }
}

async function show(req, res) {
  const { id } = req.params;
  try {
    const items = await Items.findByPk(id);
    return res.status(200).json(Items);
  } catch (e) {
    return res.status(500).json(e);
  }
}

async function update(req, res) {
  const { id } = req.params;
  try {
    const [updated] = await Items.update(req.body, { where: { id: id } });
    if (updated) {
      const items = await Items.findByPk(id);
      return res.status(200).json(items);
    }
  } catch (error) {
    return res.status(500).json("usuario não encontrado");
  }
}

async function destroy(req, res) {
  const { id } = req.params;
  try {
    const deleted = await Items.destroy({ where: { id: id } });
    if (deleted) {
      return res.status(200).json("usuario deletado");
    }

    throw new Error();
  } catch (e) {
    return res.status(500).json("usuario não encontrado");
  }
}

async function addUser(req, res) {
  const { userId, itemsId } = req.params;
  try {
    const user = await User.findByPk(userId);
    const items = await Items.findByPk(itemsId);
    await items.setUser(user);
    return res.status(200).json(items);
  } catch (error) {
    return res.status(500).json({ error });
  }
}

module.exports = {
  create,
  index,
  show,
  update,
  destroy,
  addUser,
};
