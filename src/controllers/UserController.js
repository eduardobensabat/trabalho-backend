const Items = require("../models/Items");
const User = require("../models/User");

async function create(req, res) {
  try {
    const user = await User.create(req.body);
    return res
      .status(201)
      .json({ message: "usuario cadastrado com sucesso", User: user });
  } catch (err) {
    return res.status(500).json(err);
  }
}

async function index(req, res) {
  try {
    const users = await User.findAll();
    return res
      .status(200)
      .json({ message: "todos os usuarios listados", Users: users });
  } catch (error) {
    return res.status(500).json(error);
  }
}

async function show(req, res) {
  const { id } = req.params;
  try {
    const user = await User.findByPk(id, {
      include: [{ model: Items }],
    });
  } catch (error) {
    return res.status(500).json(error);
  }
}

async function update(req, res) {
  const { id } = req.params;
  try {
    const [updated] = await User.update(req.body, { where: { id: id } });
    if (updated) {
      const user = await User.findByPk(id);
      return res.status(200).json(user);
    }
  } catch (error) {
    return res.status(500).json("usuario não encontrado");
  }
}

async function destroy(req, res) {
  const { id } = req.params;
  try {
    const deleted = await User.destroy({ where: { id: id } });
    if (deleted) {
      return res.status(200).json("usuario deletado");
    }

    throw new Error();
  } catch (e) {
    return res.status(500).json("usuario não encontrado");
  }
}

module.exports = {
  create,
  index,
  show,
  update,
  destroy,
};
