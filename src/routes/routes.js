const Express = require("express");
const router = Express();
const UserController = require("../controllers/UserController");
const ItemsController = require("../controllers/ItemsController");

router.post("/user", UserController.create);
router.get("/user/:id", UserController.show);
router.get("/user", UserController.index);
router.put("/user/:id", UserController.update);
router.delete("/user/:id", UserController.destroy);

router.post("/items", ItemsController.create);
router.get("/items/:id", ItemsController.show);
router.get("/items", ItemsController.index);
router.put("/items/:id", ItemsController.update);
router.delete("/items/:id", ItemsController.destroy);

router.put("/user/:userId/items/:itemsId", ItemsController.addUser);

module.exports = router;
